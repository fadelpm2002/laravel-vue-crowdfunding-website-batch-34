// Fadel Pramaputra Maulana

/* No 1 */
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort();

for (var i = 0; i < daftarHewan.length; i++) {
  console.log(daftarHewan[i]);
}

/* No 2 */
introduce = (data) => {
  return "Nama saya " + data.name + ", umur saya " + data.age + ", alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby;
};

var data = { name: "John", age: 30, address: "Jalan Pelesiran", hobby: "Gaming" };

var perkenalan = introduce(data);

console.log(perkenalan); // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming"

/* No 3 */
hitung_huruf_vokal = (str) => {
  var vokal = 0;
  is_vocal_func = (ch) => {
    if (ch == "a" || ch == "A") return true;
    if (ch == "i" || ch == "I") return true;
    if (ch == "u" || ch == "U") return true;
    if (ch == "e" || ch == "E") return true;
    if (ch == "o" || ch == "O") return true;
    return false;
  };
  for (var i = 0; i < str.length; i++) {
    if (is_vocal_func(str[i])) vokal++;
  }
  return vokal;
};

var hitung_1 = hitung_huruf_vokal("Muhammad");

var hitung_2 = hitung_huruf_vokal("Iqbal");

console.log(hitung_1, hitung_2); // 3 2

/* No 4 */
hitung = (val) => {
  if (val == 1) return 0;
  if (val < 1) return -1 * 2 * (val - 1) * -1;
  if (val > 1) return 2 * (val - 1);
};

console.log(hitung(0)); // -2
console.log(hitung(1)); // 0
console.log(hitung(2)); // 2
console.log(hitung(3)); // 4
console.log(hitung(5)); // 8
