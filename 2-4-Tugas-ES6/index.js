// Fadel Pramaputra Maulana

/* No 1 */
const luasSegiempat = (l, p) => {
  return l * p;
};
const kelilingSegiempat = (l, p) => {
  return 2 * (p + l);
};

console.log(kelilingSegiempat(7, 3));
console.log(luasSegiempat(7, 3));

/* No 2 */
const newFunction = (firstName, lastName) => {
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: () => {
      console.log(firstName + " " + lastName);
    },
  };
};

newFunction("William", "Imoh").fullName();
console.log(newFunction("William", "Imoh").firstName);
console.log(newFunction("William", "Imoh").lastName);
console.log(newFunction("William", "Imoh"));

/* No 3 */
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
};
const { firstName, lastName, address, hobby } = newObject;

console.log(firstName, lastName, address, hobby);

/* No 4 */
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
// const combined = west.concat(east)
const combined = [...west, ...east];

console.log(combined);

/* No 5 */
const planet = "earth";
const view = "glass";
var before = "Lorem " + view + "dolor sit amet, " + "consectetur adipiscing elit," + planet;
var after = `Lorem ${view}dolot sit amet, consectetur adipiscing elit,${planet}`;

console.log(before);
console.log(after);
