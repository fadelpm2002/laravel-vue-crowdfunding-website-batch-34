<!-- Fadel Pramaputra Maulana -->

<?php

trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlaKaki;
    public $keahlian;

    public function atraksi(){
        echo "{$this->nama} sedang {$this->keahlian} <br>";
    }
}

abstract class Fight {
    use Hewan;
    public $attackPower;
    public $defencePower;

    abstract public function getInfoHewan();

    public function diserang($hewan){
        echo "{$this->nama} sedang diserang <br>";
        $this->darah = $this->darah - $hewan->attackPower/$this->defencePower;
    }

    public function serang($hewan){
        echo "{$this->nama} sedang menyerang {$hewan->nama} <br>";
        $hewan->diserang($this);
    }

    protected function getInfo(){
        echo "Nama Hewan: {$this->nama} <br>";
        echo "Darah: {$this->darah} <br>";
        echo "Jumlah Kaki: {$this->jumlahKaki} <br>";
        echo "Keahlian: {$this->keahlian} <br>";
        echo "Attack Power: {$this->attackPower} <br>";
        echo "Defence Power: {$this->defencePower} <br>";
        echo "Atraksi: "; echo $this->atraksi();
    }
}

class Elang extends Fight {
    public function __construct($nama) {
        $this->nama = $nama;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan(){
        echo "Jenis Hewan: Elang <br>";
        $this->getInfo();
    }
}

class Harimau extends Fight {
    public function __construct($nama) {
        $this->nama = $nama;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan(){
        echo "Jenis Hewan: Harimau <br>";
        $this->getInfo();
    }
}

class Spasi {
    public static function spasiBaris() {
        echo "<br>";
    }
}

$elang1 = new Elang("elang1");
$elang1->atraksi();
$harimau1 = new Harimau("harimau1");
$harimau1->atraksi();
Spasi::spasiBaris();

$elang1->serang($harimau1);
Spasi::spasiBaris();

$harimau1->getInfoHewan();
Spasi::spasiBaris();

$elang1->serang($harimau1);
Spasi::spasiBaris();

$harimau1->getInfoHewan();
Spasi::spasiBaris();

$harimau1->serang($elang1);
Spasi::spasiBaris();

$elang1->getInfoHewan();
Spasi::spasiBaris();

$harimau1->serang($elang1);
Spasi::spasiBaris();

$elang1->getInfoHewan();
Spasi::spasiBaris();
?>